import { Container, Grid, Header, Icon } from "semantic-ui-react";
import React from "react";
import { Carousel } from "../../helpers/widgets";
import "./Home.scss";

const features = [
  {
    header: "Lorem Ipsum Dolor",
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. In
        quod vitae veritatis ratione ad ullam, similique accusamus
        ipsum suscipit recusandae autem.`,
    icon: "home"
  },
  {
    header: "Lorem Ipsum Dolor",
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. In
        quod vitae veritatis ratione ad ullam, similique accusamus
        ipsum suscipit recusandae autem.`,
    icon: "home"
  },
  {
    header: "Lorem Ipsum Dolor",
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. In
        quod vitae veritatis ratione ad ullam, similique accusamus
        ipsum suscipit recusandae autem.`,
    icon: "home"
  }
];

function Home() {
  return (
    <>
      <Carousel />
      <Container textAlign="center" className="features-container">
        <Header as="h1" className="features-header">
          LOREM IPSUM DOLOR
        </Header>
        <Grid as={Container} text className="features-overview">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. In quod vitae
          veritatis ratione ad ullam, similique accusamus ipsum suscipit
          recusandae autem.
        </Grid>
        <Grid columns="equal">
          {features.map((feature, i) => (
            <Grid.Column key={`feature-${i}`}>
              <Icon
                circular
                name={feature.icon}
                size="huge"
                className={["primary--text", "features-icons"]}
              />
              <Header as="h3">
                <Header.Content>
                  {feature.header}
                  <Header.Subheader className="features-descriptions">
                    {feature.description}
                  </Header.Subheader>
                </Header.Content>
              </Header>
            </Grid.Column>
          ))}
        </Grid>
      </Container>
    </>
  );
}

export default Home;
