import React from "react";
import { Menu, Responsive, Grid } from "semantic-ui-react";
import { NavLink, Link } from "react-router-dom";
import "./NavBar.scss";

const navItems = [
  { title: "Home", route: "/" },
  { title: "About Us", route: "/about" },
  { title: "Services", route: "/services" },
  { title: "Contact", route: "/contact" }
];

function NavBar() {
  return (
    <Grid as={Menu} text stackable className="toolbar">
      <Grid.Column
        mobile={16}
        tablet={15}
        computer={14}
        largeScreen={12}
        className="nav-container"
      >
        <Menu.Item as={Link} to="/">
          <img src="/logo.png" alt="logo" className="logo"></img>
        </Menu.Item>
        <Responsive as="nav" className="nav-items" minWidth={768}>
          {navItems.map((navItem, i) => (
            <NavLink
              to={navItem.route}
              exact
              key={`nav-item-${i}`}
              className="nav-item"
            >
              {navItem.title}
            </NavLink>
          ))}
        </Responsive>{" "}
      </Grid.Column>
    </Grid>
  );
}

export default NavBar;
